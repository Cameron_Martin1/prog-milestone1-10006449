﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task40
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = new SortedList<string, int>();


            month.Add("January", 31);
            month.Add("February", 29);
            month.Add("March", 31);
            month.Add("April", 30);
            month.Add("May", 31);
            month.Add("June", 30);
            month.Add("July", 31);
            month.Add("August", 31);
            month.Add("September", 30);
            month.Add("October", 31);
            month.Add("November", 30);
            month.Add("December", 31);


            int d = 0;

            Console.WriteLine($"Type a month with a capital first letter - followed by <ENTER>.");
            var x = Console.ReadLine();

            if (month.TryGetValue(x, out d))
            {
                if (d == 31)
                {
                    Console.WriteLine($"{x} has five Wednesdays.");
                }
                else
                {
                    Console.WriteLine($"{x} has four Wednesdays.");
                }
            }
        }
    }
}