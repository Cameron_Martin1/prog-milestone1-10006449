﻿using System;

namespace Task24
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            var space10 = "**********";
            var space5 = "     ";
            Start:

            Console.WriteLine($"{space10}{space10}{space10}{space10}");
            Console.WriteLine($"{space10}{space5}{space5}{space5}{space5}{space10}");
            Console.WriteLine($"{space10}{space5}01 Option1{space5}{space10}");
            Console.WriteLine($"{space10}{space5}02 Option2{space5}{space10}");
            Console.WriteLine($"{space10}{space5}03 Option3{space5}{space10}");
            Console.WriteLine($"{space10}{space5}{space5}{space5}{space5}{space10}");
            Console.WriteLine($"{space10}{space10}{space10}{space10}");


            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Press M to return to main menu!");

            var Selection = Console.ReadLine();
            switch (Selection)
            {
                case "m":
                    Console.Clear();
                    goto Start;
            }

        }
    }
}
