﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication13
{
    class Program
    {
        static void Main(string[] args)
        {
            string Word;

            Console.WriteLine($"Enter a Word: ");
            Word = (Console.ReadLine());

            int numberOfLetters = Word.Length;

            Console.WriteLine($"Your word was: {Word}");
            Console.WriteLine($"Amount of letters: {numberOfLetters}");


        }
    }
}
