﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task13
{
   class Program
    {
        static void Main(string[] args)
        {
            double n = 0;

            for (double input = 0; input < 3; input++)
            {
                Console.Write("Please enter an amount ($): ");

                n = n + double.Parse(Console.ReadLine());
            }
            Console.WriteLine($"The total amount plus gst = ${n + n * .15}");
        }
    }
}
