﻿using System;

namespace Birthday
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please type in your birthday below:");
            var Birthday = Console.ReadLine();
            Console.WriteLine($"Your birthday is {Birthday}!");
        }
    }
}
