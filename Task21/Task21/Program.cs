﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task21
{
    class Program
    {
        static void Main(string[] args)
        {
            var dictionary = new Dictionary<string, int>();

            dictionary.Add("January", 31);
            dictionary.Add("Febuary", 28);
            dictionary.Add("March", 31);
            dictionary.Add("April", 30);
            dictionary.Add("May", 31);
            dictionary.Add("June", 30);
            dictionary.Add("July", 31);
            dictionary.Add("August", 31);
            dictionary.Add("September", 30);
            dictionary.Add("October", 31);
            dictionary.Add("November", 30);
            dictionary.Add("December", 31);

            List<string> list = new List<string>();

            foreach (var x in dictionary)
            {
                if (x.Value == 31)
                {
                    list.Add(x.Key);

                }
            }

            Console.WriteLine($"There are {list.Count} months with 31 days");
        }
    }
}
