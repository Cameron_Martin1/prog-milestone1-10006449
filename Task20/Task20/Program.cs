﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] intArr = new int[] { 33, 45, 21, 44, 67, 87, 86 };

            var query = from i in intArr
                        group i by (i % 2 == 0) into gevenodd

                        select gevenodd;

            foreach (var va in query)
            {
                if (va.Key == false)
                {
                    int[] OddArray = va.ToArray();
                    foreach (var v in OddArray)
                    {
                        Console.Write(v); Console.Write("\t");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}