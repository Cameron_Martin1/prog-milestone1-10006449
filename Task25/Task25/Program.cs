﻿
using System;

namespace Task28
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter 1 if you like dogs or 2 if you like cats");
            var menuSelection = Console.ReadLine();

            switch (menuSelection)
            {
                case "1":
                    Console.WriteLine("Cats say meow!");
                    break;

                case "2":
                    Console.WriteLine("Dogs say woof!");
                    break;
                default:
                    Console.WriteLine("Your choice was not valid.");
                    break;
            }
        }
    }
}