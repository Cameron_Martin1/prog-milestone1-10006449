﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        List<Tuple<string, int>> list = new List<Tuple<string, int>>();
        list.Add(new Tuple<string, int>("Jack", 31));
        list.Add(new Tuple<string, int>("Michael", 32));
        list.Add(new Tuple<string, int>("Fred", 35));

        list.Sort((a, b) => a.Item2.CompareTo(b.Item2));

        foreach (var element in list)
        {
            Console.WriteLine($"Name & Age: {element}");
        }
    }
}