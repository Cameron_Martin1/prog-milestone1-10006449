﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        string[] array = new string[] { "Red", "Blue", "Yellow", "Green", "Pink" };

        Array.Sort(array);
        Array.Reverse(array);
        foreach (var str in array)

        {
            Console.WriteLine(str.ToString());
        }
    }
}