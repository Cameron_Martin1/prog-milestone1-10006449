﻿using System;

class Evencheck
{
    static void Main()
    {
        Console.WriteLine("Enter a number and the program will tell you if it's even:");
        int n = int.Parse(Console.ReadLine());

        if (n % 2 == 0)
        {
            Console.WriteLine("Yes - this number is even!");
        }
        else
        {
            Console.WriteLine("No - this number is odd!");
        }
    }
}