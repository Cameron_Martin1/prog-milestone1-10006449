﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Temp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type \"c\" to convert Celsius OR \"f\" to convert Fahrenheit");
            var tempSelection = Console.ReadLine();


            switch (tempSelection)
            {
                case "f":
                    Console.WriteLine("Enter the desired temperature to convert from Fahrenheit to Celsius");
                    int f = int.Parse(Console.ReadLine());



                    int F2C = ((f - 32) / 9) * 5;
                    Console.WriteLine($"The conversion from farenheit to celsius is: {F2C} °C");
                    break;
                default:
                    Console.WriteLine("You entered an invalid argument!! Type \"c\" for Celsius or \"f\" for Fahrenheit upon relaunching.");
                    break;


                case "c":
                    Console.WriteLine("Enter the desired temperature to convert from Celsius to Fahrenheit");
                    int c = int.Parse(Console.ReadLine());



                    int C2F = ((c * 9) / 5) + 32;
                    Console.WriteLine($"The conversion from celsius to farenheit is: {C2F} °F");
                    break;


            }
        }
    }
}
