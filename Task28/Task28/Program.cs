﻿using System;

namespace Task28
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter 3 Words - seperated with a space!");
            var words = Console.ReadLine();

            Console.WriteLine("Your selected words are as follows!");
            var set = words.Split(' ');

            foreach (var word in set)
            {
                Console.WriteLine(word);
            }
        }
    }
}
