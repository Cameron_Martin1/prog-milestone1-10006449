﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        Dictionary<string, string> values =
            new Dictionary<string, string>();

        values.Add("apple", "fruit");
        values.Add("mango", "fruit");
        values.Add("banana", "fruit");
        values.Add("watermelon", "fruit");
        values.Add("carrot", "vegetable");
        values.Add("cucumber", "vegetable");
        values.Add("beetroot", "vegetable");
        values.Add("corn", "vegetable");
        
        string test;
        if (values.TryGetValue("banana", out test)) //Enter desired fruit/vegetable from list.
        {
            Console.WriteLine($"This object is a {test}"); 
        }
    }
}